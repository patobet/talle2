'use strict';
var models = require('../models/');
const marca = models.marca;
var auto = models.auto;
const { body, validationResult, check } = require('express-validator');
const persona = models.persona;

class AutoController{

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({where:{external_id: marca_id }});
                if (marcaAux) {
                    var data = {
                        modelo: req.body.modelo,
                        anio: req.body.anio,
                        color: req.body.color,
                        precio: req.body.precio,
                        cilindraje: req.body.cilindraje,
                        id_marca: marcaAux.id,
                        placa: req.body.placa
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();

                    try {
                        await auto.create(data, {transaction });
                        console.log('guardado');
                        await transaction.commit();
                        res.json({ msg: "Se han regiustrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.error && error.error[0].message) {
                            res.json({ msg: error.error[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                    //podemos poner persona . create o save
                    // console.log(personaAux);

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({
                    msg: "Faltan datos", code: 400
                });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    
    async listarDisponibles(req, res) {
        var lista = await auto.findAll({
            where: { estado: false },
            attributes: ['modelo', 'anio', 'color', 'precio', 'cilindraje', 'placa', 'external_id'],
            include: { model: models.marca, as: "marca", attributes: ['nombre'] }
        });
        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async listarVendidos(req, res) {
        var lista = await models.sequelize.query(
            'SELECT auto.modelo, auto.placa, auto.anio, auto.color, persona.nombres, persona.apellidos, marca.nombre FROM auto INNER JOIN persona ON auto.duenio = persona.identificacion INNER JOIN marca ON auto.id_marca = marca.id',
            { type: models.Sequelize.QueryTypes.SELECT }
        );
        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        }else{res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });}
        
    }

    async contar(req, res){
        var numero = await auto.count();
        res.json({msg: "Ok", code: 200, info: numero});
    }

    async colores(req, res){
        var numero = await auto.findAll({
            attributes:['color', 'external_id']
        });
        res.json({msg: "Ok", code: 200, info: numero});
    }

    async obtenerAuto(req, res){
        const external = req.params.external;
        var lista = await auto.findOne({
            where: { external_id: external },
            attributes: ['modelo', 'external_id', 'cilindraje', 'precio', 'anio', 'placa', 'color']
        });
        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async modificar(req, res){
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var aut = await auto.findOne({ where: { external_id: req.body.external } });
            if (aut === null) {
                res.status(400);
                res.json({ msg: "No existen registros que coincidan", code: 400 });
            } else {
                var uuid = require('uuid');
                aut.modelo = req.body.modelo;
                aut.anio = req.body.anio;
                aut.placa = req.body.placa;
                aut.cilindraje = req.body.cilindraje;
                aut.precio = req.body.precio;
                aut.external_id = uuid.v4();
                var result = await aut.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se ha podido modificar sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }
    }
}

module.exports = AutoController;