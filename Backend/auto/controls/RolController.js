'use strict';
var models = require('../models/');
var rol = models.rol;
const { body, validationResult, check } = require('express-validator');

class RolController {
    async listar(req, res) {
        var lista = await rol.findAll({
            attibutes: ['nombre', 'external_id', 'estado']
        });
        res.json({ msg: "Lista vacia", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
                var data = {
                    nombre: req.body.nombre
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();

                try {
                    await rol.create(data, {transaction });
                    console.log('guardado');
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.error && error.error[0].message) {
                        res.json({ msg: error.error[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
                //podemos poner persona . create o save
                // console.log(personaAux);
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var roll = await rol.findOne({ where: { external_id: req.body.external } });
            if (roll === null) {
                res.status(400);
                res.json({ msg: "No existen registros que coincidan", code: 400 });
            } else {
                roll.nombre = req.body.nombre;
                var result = await roll.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se ha podido modificar sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }
    }

    async darBaja(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var roll = await rol.findOne({ where: { external_id: req.body.external } });
            if (roll === null) {
                res.status(400);
                res.json({ msg: "No existen registros que coincidan", code: 400 });
            } else {
                roll.estado = false;
                var result = await roll.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se ha podido modificar sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }
    }

}

module.exports = RolController;