'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var rol = models.rol;
var persona = models.persona;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class PersonaController {

    /*async listar(req, res){ 
        var lista = await persona.findAll({include: {model: models.}})
            attributes:['apellido', 'external_id', 'nombres', 'direccion', '']
        }*/

    async listar(req, res) {
        var listar = await persona.findAll({
            attributes: ['nombres', 'apellidos', 'direccion', 'identificacion', 'tipo_identificacion']
        });
        res.json({ msg: 'OK!', code: 200, info: listar });

    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external }, include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'tipo_identificacion']
        });
        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let rol_id = req.body.external_rol;
            if (rol_id != undefined) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                    };
                    console.log(claveHash(req.body.clave));
                    var data = {
                        identificacion: req.body.identificacion,
                        tipo_identificacion: req.body.tipo_identificacion,
                        apellidos: req.body.apellidos,
                        nombres: req.body.nombres,
                        direccion: req.body.direccion,
                        id_rol: rolAux.id,
                        cuenta: {
                            username: req.body.usuario,
                            correo: req.body.correo,
                            clave: claveHash(req.body.clave)
                        }
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();

                    try {
                        await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        console.log('guardado');
                        await transaction.commit();
                        res.json({ msg: "Se han regiustrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.error && error.error[0].message) {
                            res.json({ msg: error.error[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                    //podemos poner persona . create o save
                    // console.log(personaAux);

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({
                    msg: "Faltan datos", code: 400
                });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var person = await persona.findOne({ where: { external_id: req.body.external } });
            if (person === null) {
                res.status(400);
                res.json({ msg: "No existen registros que coincidan", code: 400 });
            } else {
                var uuid = require('uuid');
                person.identificacion = req.body.identificacion;
                person.tipo_identificacion = req.body.tipo_identificacion;
                person.apellidos = req.body.apellidos;
                person.nombres = req.body.nombre;
                person.direccion = req.body.direccion;
                person.external_id = uuid.v4();
                var result = await person.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se ha podido modificar sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }
    }
}
module.exports = PersonaController;