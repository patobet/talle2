var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
let jwt = require('jsonwebtoken')

const RolController = require('../controls/RolController');
var rolController = new RolController();

const PersonaController = require('../controls/PersonaController');
var personaController = new PersonaController();

const CuentaController = require('../controls/CuentaController');
var cuentaController = new CuentaController();

const FacturaController = require('../controls/FacturaController');
var facturaController = new FacturaController();

const MarcaController = require('../controls/MarcaController');
var marcaController = new MarcaController();

const AutoController = require('../controls/AutoController');
var autoController = new AutoController();

var auth = function middleware (req, res, next) {
  const token = req.headers['x-api-token'];
  //console.log(req.headers);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log('aqui',err);
        res.status(401);
        res.json({ msg: "Token no valido!", code: 401 });
      }else{
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({where: {external_id: req.decoded.external}});
        if (aux) {
          next();
        }else{
          res.status(401);
          res.json({ msg: "Token no valido!", code: 401 });
        }
      }

    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token!", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "Version": "1.0", "Que": "mas" });
});
//Login
router.post('/sesion', [
  body('email', 'Ingrese un correo valido').exists().not().isEmpty().isEmail(),
  body('clave', 'Ingrese una clave valido').exists().not().isEmpty(),
], cuentaController.sesion)
//rol
router.post('/roles/guardar', auth,[
  body('nombre', 'Ingrese el nombre del rol').exists().not().isEmpty().isLength({ min: 2, max: 20 }).withMessage("Ingrese una valor mayor a 2 y menor a 20")
], rolController.guardar);
router.get('/roles',auth, rolController.listar);
router.post('/roles/modificar',auth, rolController.modificar);
router.post('/roles/eliminar',auth, rolController.darBaja);
//persona
router.post('/persona/guardar',auth, [
  body('apellidos', 'Ingrese su apellido').exists().not().isEmpty().isLength({ min: 3, max: 50 }).withMessage("Ingrese una valor mayor a 3 y menor a 50"),
  body('nombres', 'Ingrese su nombre').exists()
], personaController.guardar);
router.post('/persona/modificar',auth, personaController.modificar);
router.get('/persona/obtener/:external',auth, personaController.obtener);
router.get('/listar',auth, personaController.listar);
//factura
router.get('/factura/listar',auth, facturaController.listar);
router.post('/factura/guardar',auth, [
  body('numero', 'Ingrese el numero de la factura').exists().not().isEmpty()
], facturaController.guardar);
//marca
router.post('/marca/guardar',auth, [
  body('nombre', 'Ingrese el nombre de la marca').exists().not().isEmpty().withMessage("Ingrese una valor")
], marcaController.guardar);
router.get('/marca/listar',auth, marcaController.listar);
router.get('/marca/contar',auth ,marcaController.contar)
//auto
router.post('/auto/guardar', auth,[
  body('modelo', 'Ingrese el nombre del modelo').exists().not().isEmpty(),
  body('anio', 'Ingrese su modelo').exists().not().isEmpty(),
  body('cilindraje', 'Ingrese su modelo').exists().not().isEmpty(),
  body('color', 'Ingrese su modelo').exists().not().isEmpty(),
  body('external_marca', 'Ingrese su modelo').exists().not().isEmpty(),
  body('placa', 'Ingrese su modelo').exists().not().isEmpty(),
  body('precio', 'Ingrese su modelo').exists().not().isEmpty(),
], autoController.guardar);
router.get('/auto/disponible', autoController.listarDisponibles);
router.get('/auto/vendido',auth, autoController.listarVendidos);
router.get('/auto/contar',auth ,autoController.contar);
router.get('/auto/colores',auth ,autoController.colores);
router.get('/auto/obtener/:external',auth, autoController.obtenerAuto);
router.post('/auto/modificar',auth,  [
  body('modelo', 'Ingrese su modelo').exists().not().isEmpty(),
  body('anio', 'Ingrese su modelo').exists().not().isEmpty(),
  body('cilindraje', 'Ingrese su modelo').exists().not().isEmpty(),
  body('color', 'Ingrese su modelo').exists().not().isEmpty(),
  body('external', 'Ingrese su modelo').exists().not().isEmpty(),
  body('placa', 'Ingrese su modelo').exists().not().isEmpty(),
  body('precio', 'Ingrese su modelo').exists().not().isEmpty(),
],autoController.modificar);



/*router.post('/sumar', function(req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if(isNaN(a)||isNaN(b)){
    res.status(400);
    res.json({"msg":"Faltan datos"});  
  }
  var c = a+b;
  
  res.status(200);
  res.json({"msg":"OK", "resp":String(c)});
});*/

module.exports = router;
