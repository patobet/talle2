'use strict';

module.exports = (sequalize, DataTypes) => {
    const factura = sequalize.define('factura', {
        numero: {type: DataTypes.INTEGER(20), defaultValue: 0 ,allowNull: false, unique: true},
        fecha: {type: DataTypes.DATE, defaultValue: sequalize.NOW ,allowNull: false},
        lugar: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        subtotal: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        iva: {type: DataTypes.DECIMAL(10,2), defaultValue: 12.0,allowNull: false},
        total: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
    }, {freezeTableName: true});

    factura.associate = function(models) {
        factura.hasMany(models.detalleFactura, {foreignKey: 'id_factura', as: 'detalleFactura'});
        factura.belongsTo(models.persona,{foreignKey: 'id_persona', as: 'persona'});
    }
    return factura;
}