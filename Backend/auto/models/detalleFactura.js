'use strict';
module.exports = (sequalize, DataTypes) => {
    const detalleFactura = sequalize.define('detalleFactura', {
        numero: {type: DataTypes.INTEGER(20), defaultValue: 0 ,allowNull: false},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, {freezeTableName: true});

    detalleFactura.associate = function(models) {
        detalleFactura.belongsTo(models.factura,{foreignKey: 'id_factura', as:'factura'});
        detalleFactura.hasMany(models.auto, {foreignKey: 'id_detalleFactura', as: 'auto'});
    }
    return detalleFactura;
}
