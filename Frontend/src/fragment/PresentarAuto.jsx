import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarAuto from "./RegistrarAuto";
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { Autos, AutosDisponibles, AutosVendidos, ModificarAuto, ObtenerAuto } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;



export const Disponibles = () => {
    const [autoEditando, setAutoEditando] = useState(null);
    const [llAuto, setLlAuto] = useState(false);
    const [modelo, setModelo] = useState('');
    const [anio, setAnio] = useState(0);
    const [color, setColor] = useState('');
    const [placa, setPlaca] = useState('');
    const [cilindraje, setCilindraje] = useState(0);
    const [precio, setPrecio] = useState('');


    const handleEditar = async (elemento) => {
        if (getToken()) {
            ObtenerAuto(elemento.external_id, getToken()).then((info) => {
                setAutoEditando(info.info);
                setModelo(info.info.modelo);
                setAnio(info.info.anio);
                setColor(info.info.color);
                setPlaca(info.info.placa);
                setCilindraje(info.info.cilindraje);
                setPrecio(info.info.precio);
            })
        } else {
            mensajes("No tiene permisos para editar")
        }
    };

    const handleGuardar = () => {
        var datos = {
            "modelo": modelo,
            "anio": anio,
            "color": color,
            "placa": placa,
            "cilindraje": cilindraje,
            "precio": precio,
            "external": autoEditando.external_id
        };
        ModificarAuto(datos, getToken()).then((info)=>{
            if (info.code == 200) {
                setAutoEditando(null);
                setModelo('');
                setAnio(0);
                setColor('');
                setPlaca('');
                setCilindraje(0);
                setPrecio(0);
                mensajes();
                setLlauto(false);
            } else {
                mensajes(info.msg, 'error');
                setLlauto(false);
                
            }
        });
        

    };


    const handleSalir = () => {
        // Limpiar el elemento en edición
        setAutoEditando(null);
        setModelo('');
        setAnio(0);
        setColor('');
        setPlaca('');
        setCilindraje(0);
        setPrecio(0);
    };
    const columnsD = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Año',
            selector: row => row.anio,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Placa',
            selector: row => row.placa,
        },
        {
            name: 'Cilindraje',
            selector: row => row.cilindraje,
        },
        {
            name: 'Marca',
            selector: row => row.marca.nombre,
        },
        {
            name: 'Acciones',
            cell: (row, index) => (
                <button type="button" onClick={() => handleEditar(row)} class="btn btn-warning">Editar</button>
            ),
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }

    ];

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llauto, setLlauto] = useState(false);

    AutosDisponibles(getToken()).then((info) => {
        if (!llauto) {
            if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {

                setData(info.info);

            }
            setLlauto(true);
        }
    })

    return (

        <div className="container ">
            {autoEditando ? (
                // Mostrar pestaña de edición
                <div className='container-fluid'>
                    <div className="col-lg-10">
                        <div className="p-6">
                            <h1 className="h4 text-gray-900 mb-5">Editar Auto</h1>
                            <label>
                                Modelo:
                                <input className="form-control form-control-user" placeholder="Ingrese el modelo"
                                    type="text"
                                    value={modelo}
                                    onChange={(e) => setModelo(e.target.value)}
                                />
                            </label>
                            <br />
                            <label>
                                Color:
                                <input
                                    className="form-control form-control-user" placeholder="Ingrese el color"
                                    type="text"
                                    value={color}
                                    onChange={(e) => setColor(e.target.value)}
                                />
                            </label>
                            <br />

                            <label>
                                Placa:
                                <input
                                    className="form-control form-control-user" placeholder="Ingrese la placa"
                                    type="text"
                                    value={placa}
                                    onChange={(e) => setPlaca(e.target.value)}
                                />
                            </label>
                            <br />
                            <label>
                                Precio:
                                <input
                                    className="form-control form-control-user" placeholder="Ingrese el precio"
                                    type="text"
                                    value={precio}
                                    onChange={(e) => setPrecio(e.target.value)}
                                />
                            </label>
                            <br />
                            <label>
                                Año:
                                <input
                                    className="form-control form-control-user" placeholder="Ingrese el precio"
                                    type="text"
                                    value={anio}
                                    onChange={(e) => setAnio(e.target.value)}
                                />
                            </label>
                            <br />
                            <label>
                                Cilindraje:
                                <input
                                    className="form-control form-control-user" placeholder="Ingrese el cilindraje"
                                    type="text"
                                    value={cilindraje}
                                    onChange={(e) => setCilindraje(e.target.value)}
                                />
                            </label>
                            <br />
                            <br />
                            {/* Agregar botón de "Guardar" */}
                            <button className="btn btn-facebook btn-user btn-block" onClick={handleGuardar}>Guardar</button>
                            <button className="btn btn-google btn-user btn-block" onClick={handleSalir}>Salir</button>
                        </div>
                    </div>
                </div>
            ) : (
                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    <div className="row ">

                        <div className="col-sm-3 mt-5 mb-4 text-gred">
                            <div className="search">
                                <form className="form-inline">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar auto" aria-label="Search" />

                                </form>
                            </div>
                        </div>
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}><h2><b>Autos disponibles</b></h2></div>
                        <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                            <Button variant="primary" onClick={handleShow}>
                                Agregar auto
                            </Button>
                        </div>
                    </div>
                    <div className="row">

                        <DataTable
                            columns={columnsD}
                            data={data}

                        />

                    </div>
                    {/* <!--- Model Box ---> */}
                    <div className="model_box">
                        <Modal
                            show={show}
                            onHide={handleClose}
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title>Agregar auto</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <RegistrarAuto />
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cerrar
                                </Button>

                            </Modal.Footer>
                        </Modal>
                    </div>

                </div>)}
        </div>
    );
}

export const Vendidos = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llauto, setLlauto] = useState(false);

    const columnsV = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Año',
            selector: row => row.anio,
        },
        {
            name: 'Dueño',
            selector: row => row.nombres + " " + row.apellidos,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Placa',
            selector: row => row.placa,
        },
        {
            name: 'Marca',
            selector: row => row.nombre,
        },
    ];

    AutosVendidos(getToken()).then((info) => {
        if (!llauto) {
            if (info.error == true && info.msg == 'Acceso denegado. Token a expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                setData(info.info);
            }
            setLlauto(true);
        }
    })

    return (

        <div className="container ">
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">

                    <div className="col-sm-3 mt-5 mb-4 text-gred">
                        <div className="search">
                            <form className="form-inline">
                                <input className="form-control mr-sm-2" type="search" placeholder="Buscar auto" aria-label="Search" />

                            </form>
                        </div>
                    </div>
                    <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}><h2><b>Autos vendidos</b></h2></div>
                    <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                    </div>
                </div>
                <div className="row">

                    <DataTable
                        columns={columnsV}
                        data={data}

                    />

                </div>
            </div>
        </div>
    );
}