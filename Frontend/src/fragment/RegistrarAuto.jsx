import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import { GuardarAuto, Marca, ObtenerColores, ObtenerMarca } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';

function RegistrarAuto() {
  const navegation = useNavigate();
    const {register, handleSubmit, formState:{errors}} = useForm();    
    const [colores, setColores] = useState([]);
    const [marcas, setMarcas] = useState([]);
    const [llmarca, setLlmarca] = useState(false);
    const [llcolor, setLlcolor] = useState(false);
    //acciones
    //submit
    const onSubmit = (data) => {
        
        var datos = {
          "modelo":data.vane,
          "cilindraje": data.cilindraje,
          "anio":data.anio,
          "external_marca": data.marca,
          "precio":data.precio,
          "color": data.color,
          "placa": data.placa
        };
        GuardarAuto(datos, getToken()).then((info) => {          
          if(info.error === true || data.color == 'Elija un color'|| data.marca == 'Elija una marca'){
            mensajes(info.msg, 'error', 'Error');
            //mensajes(info.message);            
          } else {            
            mensajes(info.msg);
            navegation('/inicio'); 
          }
        }
        );
    };
    //llamar colores
    
    if(!llmarca) {
        ObtenerMarca(getToken()).then((info) => {
            //console.log(info);
            if(info.code == 401) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setMarcas(info.info);
                console.log(info);
                setLlmarca(true);
            }
        });
    }

  return (
    <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">

                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de autos!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('vane',{required:true})} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.vane && errors.vane.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control form-control-user" placeholder="Ingrese el año" {...register('anio',{required:true})} />
                                        {errors.anio && errors.anio.type === 'required' && <div className='alert alert-danger'>Ingrese un año</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el cilindraje" {...register('cilindraje',{required:true})} />
                                        {errors.cilindraje && errors.cilindraje.type === 'required' && <div className='alert alert-danger'>Ingrese un cilindraje</div>}
                                    </div>
                                    <div className="form-group">
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el color" {...register('color',{required:true})} />
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Ingrese un color</div>}
                                    </div>

                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Seleccione un color</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la placa" {...register('placa',{required:true})}/>
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'>Ingrese una placa</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio',{required:true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/})} />
                                        {errors.precio && errors.precio.type === 'required' && <div className='alert alert-danger'>Ingrese el precio</div>}
                                        {errors.precio && errors.precio.type === 'pattern' && <div className='alert alert-danger'>Ingrese un precio valido</div>}
            
                                    </div>
                                    <div className="form-group">
                                        <select className='form-control' {...register('marca',{required:true})}>
                                            <option>Elija una marca</option>
                                            {marcas.map((aux, i) => {
                                                return (<option key={i} value={aux.external_id}>
                                                    {aux.nombre}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}
                                    </div>
                                    <hr />
                                    
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/autos/vendidos"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  );
}
export default RegistrarAuto;