const URL = "http://localhost:3001/api"

export const InicioSesion = async (data) => {
    var cabeceras = { 
        "Accept": 'application/json',
        "Content-Type": 'application/json'
     };
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();
    console.log("ok");
    console.log(datos+"  "+JSON.stringify(data));
    return datos;
}

export const Marca = async (key) => {
    var cabeceras = { 
    "x-api-token": key,
    "Accept": 'application/json',
    "Content-Type": 'application/json' };
    const datos = await (await fetch(URL + "/marca/contar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const ObtenerMarca = async (key) => {
    var cabeceras = { 
    "x-api-token": key,
    "Accept": 'application/json',
    "Content-Type": 'application/json' };
    const datos = await (await fetch(URL + "/marca/listar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const AutosCant = async(key) => {
    var cabeceras = { 
        "x-api-token": key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' };
    const datos = await (await fetch(URL + "/auto/contar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}
 

export const AutosDisponibles = async (key) => {
    var cabeceras = { 
        "Accept": 'application/json',
        "Content-Type": 'application/json' };
    const datos = await (await fetch(URL + "/auto/disponible", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}

export const AutosVendidos = async (key) => {
    var cabeceras = { 
        "x-api-token": key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' };
    const datos = await (await fetch(URL + "/auto/vendido", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos.info);
    return datos;
}

export const ObtenerColores = async(key)=>{
    const cabeceras = {
        "x-api-token":key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' 
    };
    const datos = await (await fetch(URL + "/auto/colores", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const GuardarAuto = async (data, key) => {
    const cabeceras = {
        "x-api-token":key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' 
    };
    const datos = await (await fetch(URL + "/auto/guardar", {
        method: "POST",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();

    return datos;
}

export const ObtenerAuto = async (id, key) =>{
    const cabeceras = {
        "x-api-token":key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' 
    };
    const datos = await (await fetch(URL + "/auto/obtener/"+id, {
        method: "GET",
        headers: cabeceras,
    })).json();
    return datos;
}

export const ModificarAuto = async (data, key) =>{
    const cabeceras = {
        "x-api-token": key,
        "Accept": 'application/json',
        "Content-Type": 'application/json' 
    };
    const datos = await (await fetch(URL + "/auto/modificar/", {
        method: "POST",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();
    return datos;
}